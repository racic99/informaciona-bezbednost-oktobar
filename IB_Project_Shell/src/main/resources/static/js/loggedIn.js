$(document).ready(function() { 

    $('#logout').on('click', function(event) {
        event.preventDefault();
        
        window.localStorage.setItem('user', '');
        window.location.replace('/');

    });

});