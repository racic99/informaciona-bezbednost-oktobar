package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xml.security.utils.JavaUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;

import model.mailclient.MailBody;
import support.MailHelper;
import support.MailReader;
import util.Base64;
import util.GzipUtil;
import util.KeyStoreReader;
import xml.crypto.AsymmetricKeyDecryption;
import xml.signature.VerifySignatureEnveloped;

public class ReadMailClient extends MailClient {

	public static long PAGE_SIZE = 3;
	public static boolean ONLY_FIRST_PAGE = true;

//	Kontrolna tacka
//	private static final String IV1_FILE = "./data/iv1.bin";
//	private static final String IV2_FILE = "./data/iv2.bin";
//	private static final String JKS_RECIEVER = "./data/recieverib@gmail.com.jks";
	
	public static void main(String[] args) throws Exception, IOException, InvalidKeyException,
	NoSuchAlgorithmException, InvalidKeySpecException, IllegalBlockSizeException,
	BadPaddingException, MessagingException, NoSuchPaddingException,
	InvalidAlgorithmParameterException {
		
        String sender = "";
        String reciever = "";
		
		//Postavljamo providera
		Security.addProvider(new BouncyCastleProvider());
		
        // Build a new authorized API client service.
        Gmail service = getGmailService();
        ArrayList<MimeMessage> mimeMessages = new ArrayList<MimeMessage>();
        
        String user = "me";
        String query = "is:unread label:INBOX";
        
        List<Message> messages = MailReader.listMessagesMatchingQuery(service, user, query, PAGE_SIZE, ONLY_FIRST_PAGE);
        for(int i=0; i<messages.size(); i++) {
        	Message fullM = MailReader.getMessage(service, user, messages.get(i).getId());
        	
        	MimeMessage mimeMessage;
			try {
				
				mimeMessage = MailReader.getMimeMessage(service, user, fullM.getId());
				
				sender = mimeMessage.getHeader("From", null);
				reciever = mimeMessage.getHeader("To", null);
				
				mimeMessage = MailReader.getMimeMessage(service, user, fullM.getId());
				
				System.out.println("\n Message number " + i);
				System.out.println("From: " + mimeMessage.getHeader("From", null));
				System.out.println("Subject: " + mimeMessage.getSubject());
				System.out.println("Body: " + MailHelper.getText(mimeMessage));
				System.out.println("\n");
				
				mimeMessages.add(mimeMessage);
	        
			} catch (MessagingException e) {
				e.printStackTrace();
			}	
        }
        
        System.out.println("Select a message to decrypt:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	        
	    String answerStr = reader.readLine();
	    Integer answer = Integer.parseInt(answerStr);
	    
		MimeMessage chosenMessage = mimeMessages.get(answer);
		
	    // Cuvanje attachmenta iz poruke u data folder
		String contentType = chosenMessage.getContentType();

		if (contentType.contains("multipart")) {
			Multipart multiPart = (Multipart) chosenMessage.getContent();
			for (int i = 0; i < multiPart.getCount(); i++) {
				MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(i);
				if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
					part.saveFile(new File(part.getFileName()));
				}
			}
		}	
	    
		AsymmetricKeyDecryption.testIt(sender, reciever);
		
		boolean verify = VerifySignatureEnveloped.testIt(sender);

        final String XML_DEC = "./data/" + sender + "_dec.xml";
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document xmlFile = db.parse(new File(XML_DEC));
			
		if (verify == true) {
			Node message = xmlFile.getFirstChild();
			NodeList list = message.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node subject_body = list.item(i);
				if(subject_body.getNodeName() == "subject") {
					System.out.println("\n\n");
					System.out.println("Subject: " + subject_body.getTextContent());
				}
				if(subject_body.getNodeName() == "body") {
					System.out.println("\n");
					System.out.println("Body: " + subject_body.getTextContent());
				}
			}

		} else {
			System.out.println("Verifikacija neuspela!");
		}
		
        // Brisanje nepotrebnih fajlova nakon slanja poruke 
        Path XML_DEC_PATH = Paths.get(XML_DEC);
        Files.delete(XML_DEC_PATH);
        
        final String XML_ENC = "./data/" + sender + "_enc.xml";
        
        Path XML_ENC_PATH = Paths.get(XML_ENC);
        Files.delete(XML_ENC_PATH);
		
		/* Kontrolna tacka
		
		MailBody mb = new MailBody(MailHelper.getText(chosenMessage));
		
		KeyStore ks = KeyStoreReader.KeyStoreRead(JKS_RECIEVER, "fakultet");
		PrivateKey privatekey = KeyStoreReader.getPrivKey(ks, "recieverib@gmail.com", "fakultet");
			
		//dekriptovanje tajnog kljuca
		Cipher rsaCipherDec = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
			
		//inicijalizacija za dekriptovanje
		//dekriptovanje se vrsi privatnim kljucem
		rsaCipherDec.init(Cipher.DECRYPT_MODE, privatekey);

		//dekriptovanje
		byte[] secretKeyBytes = rsaCipherDec.doFinal(mb.getEncKeyBytes());

		Cipher aesCipherDec = Cipher.getInstance("AES/CBC/PKCS5Padding");
		SecretKey secretKey = new SecretKeySpec(secretKeyBytes, "AES");
		
		byte[] iv1 = JavaUtils.getBytesFromFile(IV1_FILE);
		IvParameterSpec ivParameterSpec1 = new IvParameterSpec(iv1);
		aesCipherDec.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec1);
		
		byte[] bodyEnc = mb.getEncMessageBytes();
		
		//dekompresovanje i dekriptovanje teksta poruke
		String receivedBodyTxt = new String(aesCipherDec.doFinal(bodyEnc));
		String decompressedBodyText = GzipUtil.decompress(Base64.decode(receivedBodyTxt));
		System.out.println("Body text: " + decompressedBodyText);
		
		
		byte[] iv2 = JavaUtils.getBytesFromFile(IV2_FILE);
		IvParameterSpec ivParameterSpec2 = new IvParameterSpec(iv2);
		//inicijalizacija za dekriptovanje
		aesCipherDec.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec2);
		
		//dekompresovanje i dekriptovanje subjecta
		String decryptedSubjectTxt = new String(aesCipherDec.doFinal(Base64.decode(chosenMessage.getSubject())));
		String decompressedSubjectTxt = GzipUtil.decompress(Base64.decode(decryptedSubjectTxt));
		System.out.println("Subject text: " + new String(decompressedSubjectTxt));
		
		*/
	}
}
