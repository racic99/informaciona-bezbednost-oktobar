package ib.project.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "accounts")
public class Account implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "account_username", unique = true, nullable = false, length=40)
	private String username;

	@Column(name = "account_password", unique = false, nullable = false, length=20)
	private String password;


	
	public Account(Integer id, String username, String password) {

		this.id = id;

		this.username = username;

		this.password = password;

	}

	public Account() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

