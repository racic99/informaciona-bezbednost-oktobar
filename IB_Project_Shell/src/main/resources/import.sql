DROP SCHEMA IF EXISTS iboktobar;
CREATE SCHEMA iboktobar DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE iboktobar;
create table accounts(
 account_id bigint auto_increment,
 account_username varchar(40),
 account_password varchar(20),
 primary key (account_id)
);
insert into accounts(account_username,account_password) values ("ker","ker");
insert into accounts(account_username,account_password) values ("123","123");