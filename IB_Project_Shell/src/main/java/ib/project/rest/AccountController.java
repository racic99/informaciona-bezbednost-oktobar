package ib.project.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ib.project.model.Account;
import ib.project.model.AccountDTO;
import ib.project.serviceInterface.AccountServiceInterface;

@RestController
@RequestMapping(value="/accounts")
public class AccountController {
	
	@Autowired
    private AccountServiceInterface accountService;
	
	@GetMapping(value = "/login/{username}/{password}")
	public ResponseEntity<AccountDTO> login(@PathVariable("username") String username, @PathVariable("password") String password){
		System.out.println("LOGIN..........");
		Account account = accountService.findByUsernameAndPassword(username, password);
		if (account == null) {
			return new ResponseEntity<AccountDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<AccountDTO>(new AccountDTO(account), HttpStatus.OK);
	}
	
	@PostMapping(value = "/register/{username}/{password}")
	public ResponseEntity<Void> addAccount(@PathVariable("username") String username, @PathVariable("password") String password){
		System.out.println("REGISTRATION.....");
		
		Account account = new Account();
		account.setUsername(username);
		account.setPassword(password);
		
		accountService.save(account);	
		
		return new ResponseEntity<Void>(HttpStatus.OK);

	}	
	
}
