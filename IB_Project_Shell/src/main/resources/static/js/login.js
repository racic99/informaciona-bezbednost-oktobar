$(document).ready(function() { 
	var usernameInput = $('#usernameInput');
	var passwordInput = $('#passwordInput');
	var pogresniPodaci = $('#pogresniPodaci');
	var praznaPolja = $('#praznaPolja');
	pogresniPodaci.hide();
	praznaPolja.hide();

    usernameInput.focus();

    if (window.localStorage.getItem('user') != '') {
        window.location.replace('/loggedIn.html');
    }

    $('#submitButton').on('click', function(event) {
		var username = usernameInput.val();
		var password = passwordInput.val();	

		if (username == '' && password == '') {
			praznaPolja.show();
			praznaPolja[0].innerHTML = "Morate uneti korisničko ime i lozinku";
			setTimeout(function () {
				praznaPolja.hide();
			}, 3000);
			event.preventDefault();
			return false;
		} else if (username == '') {
			praznaPolja.show();
			praznaPolja[0].innerHTML = "Morate uneti korisničko ime";
			setTimeout(function () {
				praznaPolja.hide();
			}, 3000);
			event.preventDefault();
			return false;
		} else if (password == '') {
			praznaPolja[0].innerHTML = "Morate uneti lozinku";
			praznaPolja.show();
			setTimeout(function () {
				praznaPolja.hide();
			}, 3000);
			event.preventDefault();
			return false;
		}
        
        $.ajax('https://localhost:8443/accounts/login/' + username + '/' + password, {
            type: 'GET',
            statusCode: {
               200: function () {

                window.localStorage.setItem('user', username);
                window.location.replace('/loggedIn.html');

               },
               404: function () {

                pogresniPodaci.show();
				setTimeout(function () {
					pogresniPodaci.hide();
				}, 3000);
				usernameInput.val('');
				passwordInput.val('');
                return;
                
               }
            },
         });
		
		event.preventDefault();
		return false;
	});
    
});