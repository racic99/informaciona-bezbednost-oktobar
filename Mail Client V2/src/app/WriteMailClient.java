package app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.mail.internet.MimeMessage;

import org.apache.xml.security.utils.JavaUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.google.api.services.gmail.Gmail;

import model.mailclient.MailBody;
import util.Base64;
import util.GzipUtil;
import util.IVHelper;
import util.KeyStoreReader;
import xml.CreateXML;
import xml.crypto.AsymmetricKeyEncryption;
import xml.signature.SignEnveloped;
import support.MailHelper;
import support.MailWritter;

public class WriteMailClient extends MailClient {

//	Kontrolna tacka
//	private static final String IV1_FILE = "./data/iv1.bin";
//	private static final String IV2_FILE = "./data/iv2.bin";
//	private static final String JKS_SENDER = "./data/bezbednostoktobar@gmail.com.jks";
	
	public static void main(String[] args) {
		
		//Postavljamo providera
		Security.addProvider(new BouncyCastleProvider());
		
        try {
        	Gmail service = getGmailService();
        	
        	System.out.println("Insert sender's email:");
        	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String sender = reader.readLine();
            
        	System.out.println("Insert a reciever:");
            String reciever = reader.readLine();
        	
            System.out.println("Insert a subject:");
            String subject = reader.readLine();
            
            
            System.out.println("Insert body:");
            String body = reader.readLine();
            
        	CreateXML.CreateXMLFile(sender, subject, body);
        	
        	SignEnveloped.testIt(sender);
        	
        	AsymmetricKeyEncryption.testIt(sender, reciever);
        	
        	final String XML_ENC = "./data/" + sender + "_enc.xml";
        	
            MimeMessage mimeMessage = MailHelper.createMimeMessage(reciever, XML_ENC);
            MailWritter.sendMessage(service, "me", mimeMessage);
            
            // Brisanje nepotrebnih fajlova nakon slanja poruke
            final String XML = "./data/" + sender + ".xml";
            
            Path XML_PATH = Paths.get(XML);
            Files.delete(XML_PATH);
            
            final String XML_SIGNED = "./data/" + sender + "_signed.xml";
            
            Path XML_SIGNED_PATH = Paths.get(XML_SIGNED);
            Files.delete(XML_SIGNED_PATH);

            Path XML_ENC_PATH = Paths.get(XML_ENC);
            Files.delete(XML_ENC_PATH);
            
            /* Kontrolna tacka
            
            //Compression
            String compressedSubject = Base64.encodeToString(GzipUtil.compress(subject));
            String compressedBody = Base64.encodeToString(GzipUtil.compress(body));
            
            //Key generation
            KeyGenerator keyGen = KeyGenerator.getInstance("AES"); 
			SecretKey secretKey = keyGen.generateKey();
			Cipher aesCipherEnc = Cipher.getInstance("AES/CBC/PKCS5Padding");
			
			//inicijalizacija za sifrovanje 
			IvParameterSpec ivParameterSpec1 = IVHelper.createIV();
			aesCipherEnc.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec1);
			
			
			//sifrovanje
			byte[] ciphertext = aesCipherEnc.doFinal(compressedBody.getBytes());
			String ciphertextStr = Base64.encodeToString(ciphertext);
			System.out.println("Kriptovan tekst: " + ciphertextStr);
			
			
			//inicijalizacija za sifrovanje 
			IvParameterSpec ivParameterSpec2 = IVHelper.createIV();
			aesCipherEnc.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec2);
			
			byte[] ciphersubject = aesCipherEnc.doFinal(compressedSubject.getBytes());
			String ciphersubjectStr = Base64.encodeToString(ciphersubject);
			System.out.println("Kriptovan subject: " + ciphersubjectStr);
			
			KeyStore ks = KeyStoreReader.KeyStoreRead(JKS_SENDER, "fakultet");
			
			Certificate cert = KeyStoreReader.getCert(ks, "recieverib@gmail.com");

			PublicKey publicKey = KeyStoreReader.getPubKey(cert);
			
			//kriptovanje tajnog kljuca
			Cipher rsaCipherEnc = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
			
			//inicijalizacija za kriptovanje, 
			//kriptovanje se vrsi javnim kljucem, a dekriptovanje privatnim
			rsaCipherEnc.init(Cipher.ENCRYPT_MODE, publicKey);
			
			//kriptovanje
			byte[] cipherKey = rsaCipherEnc.doFinal(secretKey.getEncoded());
			System.out.println("Kriptovan kljuc: " + Base64.encodeToString(cipherKey));

			MailBody mb = new MailBody(ciphertextStr,
					ivParameterSpec1.getIV(), ivParameterSpec2.getIV(),cipherKey);

			
			//snimaju se bajtovi kljuca i IV.
			JavaUtils.writeBytesToFilename(IV1_FILE, ivParameterSpec1.getIV());
			JavaUtils.writeBytesToFilename(IV2_FILE, ivParameterSpec2.getIV());
			
        	MimeMessage mimeMessage = MailHelper.createMimeMessage(reciever, ciphersubjectStr, mb.toCSV());
        	MailWritter.sendMessage(service, "me", mimeMessage);
        	
        	*/
        	
        }catch (Exception e) {
        	e.printStackTrace();
		}
	}
}
