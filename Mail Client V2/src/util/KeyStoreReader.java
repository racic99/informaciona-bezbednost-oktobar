package util;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;

public class KeyStoreReader {
	
	  public static KeyStore KeyStoreRead(String KeyStoreFilePath, String password) 
			  throws Exception {
		  	try {
			    FileInputStream is = new FileInputStream(KeyStoreFilePath);

			    KeyStore keystore = KeyStore.getInstance("JKS", "SUN");
			    keystore.load(is, password.toCharArray());
			    
			    return keystore;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

		  }
	  
	  public static Certificate getCert(KeyStore keystore, String alias) throws KeyStoreException {
		  
		  try {
			    // Get certificate of public key
			    Certificate cert = keystore.getCertificate(alias);
			    return cert;
		} catch (KeyStoreException e) {
			e.printStackTrace();
			return null;
		}

	  }
	  
	  public static PublicKey getPubKey(Certificate cert) {
			  // Get public key
			  PublicKey publicKey = cert.getPublicKey();
			  return publicKey;

	  }
	  
		public static PrivateKey getPrivKey(KeyStore keystore, String alias, String password)
		throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException{
			try {
				
				return (PrivateKey)keystore.getKey(alias, password.toCharArray());
			} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
				e.printStackTrace();
				return null;
			}
		}


}
