$(document).ready(function() { 

    var usernameInput = $('#userInput');
    var passwordInput = $('#passInput');
    var passRepeatInput = $('#passRepeatInput');
	var praznaPolja = $('#praznaPolja');
	praznaPolja.hide();
	
    usernameInput.focus();

    $('#submitRegButton').on('click', function(event) {
		var username = usernameInput.val();
        var password = passwordInput.val();	
        var passRepeat = passRepeatInput.val();

		if (username == '') {
			praznaPolja.show();
			praznaPolja[0].innerHTML = "Morate uneti korisničko ime";
			setTimeout(function () {
				praznaPolja.hide();
			}, 3000);
			event.preventDefault();
			return false;
		} else if (password == '') {
			praznaPolja[0].innerHTML = "Morate uneti lozinku";
			praznaPolja.show();
			setTimeout(function () {
				praznaPolja.hide();
			}, 3000);
			event.preventDefault();
			return false;
		} else if (passRepeat == '') {
			praznaPolja.show();
			praznaPolja[0].innerHTML = "Morate uneti ponovljenu lozinku";
			setTimeout(function () {
				praznaPolja.hide();
			}, 3000);
			event.preventDefault();
			return false;
		} else if (password != passRepeat) {
			praznaPolja.show();
			praznaPolja[0].innerHTML = "Lozinke se ne poklapaju";
			setTimeout(function () {
				praznaPolja.hide();
			}, 3000);
			event.preventDefault();
			return false;
        }

		$.post('https://localhost:8443/accounts/register/' + username + '/' + password, function() { 

            window.location.replace('/');

        });
        
		event.preventDefault();
		return false;
	});
    
});