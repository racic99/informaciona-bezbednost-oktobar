package ib.project.model;

import java.io.Serializable;

public class AccountDTO implements Serializable {
	private Integer id;
    private String username;
    private String password;

    public AccountDTO() {
    }

    public AccountDTO(Integer id, String username, String token, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
    
    public AccountDTO(Account account) {
    	this.setId(account.getId());
    	this.setUsername(account.getUsername());
    	this.setPassword(account.getPassword());
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
}

