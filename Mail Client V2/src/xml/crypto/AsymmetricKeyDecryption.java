package xml.crypto;

import java.io.File;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xml.security.encryption.XMLCipher;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import util.KeyStoreReader;

//Dekriptuje tajni kljuc privatnim kljucem
//Tajnim kljucem dekriptuje podatke
public class AsymmetricKeyDecryption {

	static {
		// staticka inicijalizacija
		Security.addProvider(new BouncyCastleProvider());
		org.apache.xml.security.Init.init();
	}

	public static void testIt(String sender, String reciever) throws Exception {
		final String IN_FILE = "./data/" + sender + "_enc.xml";
		final String OUT_FILE = "./data/" + sender + "_dec.xml";
		final String JKS_RECIEVER = "./data/" + reciever + ".jks";
		
		// ucitava se dokument
		Document doc = loadDocument(IN_FILE);

		KeyStore ks = KeyStoreReader.KeyStoreRead(JKS_RECIEVER, "fakultet");
		
		//ucitava se privatni kljuc
		PrivateKey pk = KeyStoreReader.getPrivKey(ks, reciever, "fakultet");

		// kriptuje se dokument
		System.out.println("Decrypting....");
		doc = decrypt(doc, pk);

		// snima se dokument
		saveDocument(doc, OUT_FILE);
		System.out.println("Encryption done");
	}

	/**
	 * Kreira DOM od XML dokumenta
	 */
	private static Document loadDocument(String file) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(new File(file));

			return document;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Snima DOM u XML fajl
	 */
	private static void saveDocument(Document doc, String fileName) {
		try {
			File outFile = new File(fileName);
			FileOutputStream f = new FileOutputStream(outFile);

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);

			transformer.transform(source, result);

			f.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Kriptuje sadrzaj prvog elementa message
	 */
	private static Document decrypt(Document doc, PrivateKey privateKey) {

		try {
			// cipher za dekritpovanje XML-a
			XMLCipher xmlCipher = XMLCipher.getInstance();

			// inicijalizacija za dekriptovanje
			xmlCipher.init(XMLCipher.DECRYPT_MODE, null);

			// postavlja se kljuc za dekriptovanje tajnog kljuca
			xmlCipher.setKEK(privateKey);

			// trazi se prvi EncryptedData element
			NodeList encDataList = doc.getElementsByTagNameNS("http://www.w3.org/2001/04/xmlenc#", "EncryptedData");
			Element encData = (Element) encDataList.item(0);

			// dekriptuje se
			// pri cemu se prvo dekriptuje tajni kljuc, pa onda njime podaci
			xmlCipher.doFinal(doc, encData);

			return doc;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
// proba desifrovanje poruke

//	public static void main(String[] args) {
//		AsymmetricKeyDecryption decrypt = new AsymmetricKeyDecryption();
//		try {
//			decrypt.testIt("bezbednostoktobar@gmail.com", "recieverib@gmail.com");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
