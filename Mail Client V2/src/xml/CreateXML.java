package xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreateXML {
	
// proba kreiranje xml fajla
	
//	public static void main(String[] args) {
//		try {
//			CreateXMLFile("bezbednostoktobar@gmail.com", "naslov proba", "kreiranje xml fajla");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	  public static void CreateXMLFile(String sender, String subject, String body) 
			  throws Exception {
		  
		  String newXMLFilePath = "./data/" + sender + ".xml";
		  
		  DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		  DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		  
		  Document document = documentBuilder.newDocument();
		  
		  Element element = document.createElement("message");
		  document.appendChild(element);
		  
		  Element subjectElement = document.createElement("subject");
		  subjectElement.appendChild(document.createTextNode(subject));
		  element.appendChild(subjectElement);
		  
		  Element bodyElement = document.createElement("body");
		  bodyElement.appendChild(document.createTextNode(body));
		  element.appendChild(bodyElement);
		  
		  TransformerFactory transformerFactory = TransformerFactory.newInstance();
		  Transformer transformer = transformerFactory.newTransformer();
		  transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		  
		  DOMSource domSource = new DOMSource(document);
		  
		  StreamResult streamResult = new StreamResult(new File(newXMLFilePath));
		  
		  transformer.transform(domSource, streamResult);
	  }

}
