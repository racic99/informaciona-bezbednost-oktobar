package ib.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ib.project.model.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {

	Account findByUsernameAndPassword(String username, String password);

}

