package ib.project.serviceInterface;

import ib.project.model.Account;

public interface AccountServiceInterface {

	Account save(Account account);
	
	Account findByUsernameAndPassword(String username, String password);
	
}

